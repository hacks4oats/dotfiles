local on_attach = require("plugins.configs.lspconfig").on_attach
local capabilities = require("plugins.configs.lspconfig").capabilities

local lspconfig = require "lspconfig"

-- if you just want default config for the servers then put them in a table
local servers = { "html", "cssls", "tsserver", "clangd", "solargraph", "gopls" }

for _, lsp in ipairs(servers) do
  lspconfig[lsp].setup {
    on_attach = on_attach,
    capabilities = capabilities,
  }
end

local gopls_settings = {}

gopls_settings["gofumpt"] = true
gopls_settings["local"] = "gitlab.com/gitlab-org"
gopls_settings["staticcheck"] = true

lspconfig.gopls.setup{
  settings = {
    gopls = gopls_settings,
  }
}

lspconfig.yamlls.setup {
  on_attach = on_attach,
  capabilities = capabilities,
  settings = {
    redhat = { telemetry = { enabled = false } },
    yaml = { format = true, bracketSpacing = true, keyOrdering = false, schemaStore = { enable = true } },
  }
}
