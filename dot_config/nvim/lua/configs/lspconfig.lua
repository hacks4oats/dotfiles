-- EXAMPLE 
local on_attach = require("nvchad.configs.lspconfig").on_attach
local on_init = require("nvchad.configs.lspconfig").on_init
local capabilities = require("nvchad.configs.lspconfig").capabilities

local lspconfig = require "lspconfig"
local servers = {
  "bashls",
  "clangd",
  "cssls",
  "dockerls",
  "html",
  "jsonls",
  "ruby_lsp",
  "tsserver",
}
-- lsps with default config
for _, lsp in ipairs(servers) do
  lspconfig[lsp].setup {
    on_attach = on_attach,
    on_init = on_init,
    capabilities = capabilities,
  }
end

-- typescript
lspconfig.tsserver.setup {
  on_attach = on_attach,
  on_init = on_init,
  capabilities = capabilities,
}

-- yaml
lspconfig.yamlls.setup {
  on_attach = on_attach,
  on_init = on_init,
  capabilities = capabilities,
  settings = {
    redhat = { telemetry = { enabled = false } },
    yaml = { format = true, bracketSpacing = true, keyOrdering = false, schemaStore = { enable = true } },
  },
}

local gopls_settings = {}

gopls_settings["gofumpt"] = false
gopls_settings["local"] = "gitlab.com/gitlab-org"
gopls_settings["staticcheck"] = true

lspconfig.gopls.setup {
  on_attach = on_attach,
  on_init = on_init,
  capabilities = capabilities,
  settings = {
    gopls = gopls_settings,
  },
}
