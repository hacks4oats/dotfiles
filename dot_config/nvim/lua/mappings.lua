require "nvchad.mappings"

-- add yours here

local map = vim.keymap.set

map("n", ";", ":", { desc = "CMD enter command mode" })
map("i", "jk", "<ESC>")
map("n", "<leader>fs", ":Telescope lsp_document_symbols <cr>", { desc = "telescope find document symbols" })

-- map({ "n", "i", "v" }, "<C-s>", "<cmd> w <cr>")
